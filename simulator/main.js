"use strict";

const fs = require("fs");
const workerThreads = require("node:worker_threads");
let environments = fs.readdirSync("./environments");
let agents = fs.readdirSync("./agents");

let results = [];
let maxResults = agents.length * environments.length;
let totalScorePerAgent = {};

function onMessage(newResult)
{
    results.push(newResult);
    totalScorePerAgent[newResult.result.agent] += newResult.result.score;
    if(results.length !== maxResults)
        return null;
    results = results.map(r => r.result)
                     .sort((l, r) => [l.environment, l.agent].join("_").localeCompare([r.environment, r.agent].join("_")))
                     .filter(r => r.status === "LOST");
    console.log(results);
    console.log(totalScorePerAgent);
}

environments.forEach(environment =>
{
    agents.forEach(agent =>
    {
        if(!totalScorePerAgent.hasOwnProperty(agent))
            totalScorePerAgent[agent] = 0;
        const w = new workerThreads.Worker("./SimulatorEntry.js", { argv: [environment, agent], stdout: false, stderr: false });
        w.on("message", onMessage);
    })
});


