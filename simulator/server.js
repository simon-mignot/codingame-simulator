const fs = require("fs");
const path = require("node:path");

const express = require("express");
const morgan = require("morgan");
const app = express();
const simulate = require("./simulate");
const cors = require('cors');
const EventEmitter = require("events");

const Stream = new EventEmitter();
const port = 3000;

app.use(express.json());
app.use(cors());
app.use(morgan("combined"));

app.get("/environments", (req, res) =>
{
    let environments = fs.readdirSync("./environments");
    res.send(environments);
});

app.post("/simulate", (req, res) =>
{
    let environments = req.body.environments;
    let code = req.body.code;
    let codePath = path.join(__dirname, "tmpagents", "server_agent.js");

    if(!Array.isArray(environments))
        res.status(400).json({ error: "Environments invalid.", value: environments });
    if(typeof (code) !== "string")
        res.status(400).json({ error: "Code invalid.", value: code });

    environments = environments.map(n => path.join(__dirname, "environments", n)).filter(env => fs.existsSync(env));
    if(environments.length === 0)
    {
        res.status(404).json({ error: "Environments not found.", value: environments });
        return;
    }
    fs.writeFileSync(codePath, code, {});


    simulate.run(codePath, environments, (result, err) =>
    {
        Stream.emit("push", "simulation_result", result);
        res.send(result);
    });
});

app.get("/sse", (req, res) =>
{
    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Content-Type', 'text/event-stream');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Connection', 'keep-alive');
    res.setHeader('X-Accel-Buffering', 'no');
    res.status(200).flushHeaders();

    const finish = () => res.end();
    res.on("close", finish);
    res.on("error", finish);

    Stream.on("push", (event, data) =>
    {
        res.write("data: " + JSON.stringify({ event, data }) + "\n");
        res.write("\n\n")
    });
});

app.listen(port, () =>
{
    console.log(`Example app listening on port ${port}`);
});

