"use strict";

const workerThreads = require("node:worker_threads");

let results = {};
let maxResults = null;

function onMessage(args, finishCallback, newResult)
{
    results[args.environment.split("/").pop()] = newResult;
    if(Object.keys(results).length !== maxResults)
        return null;
    finishCallback(results);
}
function onError(args, finishCallback, err)
{
    console.log({ args, err });
    results[args.environment.split("/").pop()] = JSON.parse(JSON.stringify(err, Object.getOwnPropertyNames(err)));
    if(Object.keys(results).length !== maxResults)
        return null;
    finishCallback(results);
}

module.exports.run = function(agent, environments, callback)
{
    results = {};
    maxResults = environments.length;
    environments.forEach(environment =>
    {
        const w = new workerThreads.Worker("./src/simulator/SimulatorEntry.js",
        {
                argv: [environment, agent],
                stdout: false,
                stderr: false
        });
        w.on("message", onMessage.bind(null, { environment, agent }, callback));
        w.on("error", onError.bind(null, { environment, agent }, callback));
    });
}

