function readInputs()
{
    let humans = [];
    let zombies = [];

    var inputs = readline().split(' ');
    const x = parseInt(inputs[0]);
    const y = parseInt(inputs[1]);
    const humanCount = parseInt(readline());
    for (let i = 0; i < humanCount; i++) {
        var inputs = readline().split(' ');
        const humanId = parseInt(inputs[0]);
        const humanX = parseInt(inputs[1]);
        const humanY = parseInt(inputs[2]);
        humans.push({id: humanId, x: humanX, y: humanY});
    }
    const zombieCount = parseInt(readline());
    for (let i = 0; i < zombieCount; i++) {
        var inputs = readline().split(' ');
        const zombieId = parseInt(inputs[0]);
        const zombieX = parseInt(inputs[1]);
        const zombieY = parseInt(inputs[2]);
        const zombieXNext = parseInt(inputs[3]);
        const zombieYNext = parseInt(inputs[4]);
        zombies.push({id: zombieId, x: zombieX, y: zombieY, nextX: zombieXNext, nextY: zombieYNext});
    }
    return { x, y, humans, zombies };
}

function distance(x1, y1, x2, y2)
{
    return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
}

while(true)
{
    let { x, y, humans, zombies } = readInputs();
    let savableHumans = humans.map(human =>
    {
        human.closestZombie = zombies.map(zombie => ({...zombie, distance: distance(human.x, human.y, zombie.x, zombie.y)}))
                                     .sort((l, r) => l.distance - r.distance)[0].distance;
        human.deadIn = human.closestZombie / 400;
        human.savedIn = distance(human.x, human.y, x, y) / 1000;
        return human;
    }).filter(human => human.savedIn < human.deadIn);

    if(savableHumans.length > 0)
        console.log(savableHumans[0].x, savableHumans[0].y);
    else
        console.log(humans[0].x, humans[0].y);
}