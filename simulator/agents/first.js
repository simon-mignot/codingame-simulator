function readInputs()
{
    let humans = [];
    let zombies = [];

    var inputs = readline().split(' ');
    const x = parseInt(inputs[0]);
    const y = parseInt(inputs[1]);
    const humanCount = parseInt(readline());
    for (let i = 0; i < humanCount; i++) {
        var inputs = readline().split(' ');
        const humanId = parseInt(inputs[0]);
        const humanX = parseInt(inputs[1]);
        const humanY = parseInt(inputs[2]);
        humans.push({id: humanId, x: humanX, y: humanY});
    }
    const zombieCount = parseInt(readline());
    for (let i = 0; i < zombieCount; i++) {
        var inputs = readline().split(' ');
        const zombieId = parseInt(inputs[0]);
        const zombieX = parseInt(inputs[1]);
        const zombieY = parseInt(inputs[2]);
        const zombieXNext = parseInt(inputs[3]);
        const zombieYNext = parseInt(inputs[4]);
        zombies.push({id: zombieId, x: zombieXNext, y: zombieYNext, nextX: zombieXNext, nextY: zombieYNext});
        //zombies.push({id: zombieId, x: zombieX, y: zombieY, nextX: zombieXNext, nextY: zombieYNext});
    }
    return { x, y, humans, zombies };
}

while(true)
{
    let { x, y, humans, zombies } = readInputs();
    console.log(zombies[0].nextX, zombies[0].nextY);
}