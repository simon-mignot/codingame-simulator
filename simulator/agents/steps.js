function readInputs()
{
    let humans = [];
    let zombies = [];

    var inputs = readline().split(" ");
    const x = parseInt(inputs[0]);
    const y = parseInt(inputs[1]);
    const humanCount = parseInt(readline());
    for(let i = 0; i < humanCount; i++)
    {
        var inputs = readline().split(" ");
        const humanId = parseInt(inputs[0]);
        const humanX = parseInt(inputs[1]);
        const humanY = parseInt(inputs[2]);
        humans.push({id: humanId, x: humanX, y: humanY});
    }
    const zombieCount = parseInt(readline());
    for(let i = 0; i < zombieCount; i++)
    {
        var inputs = readline().split(" ");
        const zombieId = parseInt(inputs[0]);
        const zombieX = parseInt(inputs[1]);
        const zombieY = parseInt(inputs[2]);
        const zombieXNext = parseInt(inputs[3]);
        const zombieYNext = parseInt(inputs[4]);
        zombies.push({id: zombieId, x: zombieX, y: zombieY, nextX: zombieXNext, nextY: zombieYNext});
        //zombies.push({id: zombieId, x: zombieX, y: zombieY, nextX: zombieXNext, nextY: zombieYNext});
    }
    return {x, y, humans, zombies};
}


const env = parseInt(process.argv[2].split("/").pop().split(".").shift()) - 1;
let moves =
[
// 1: max 10
    [{type: "human", id: 0, repeat: 9}],

// 2: max 80
    [
        {type: "pos", x: 2700, y: 5100, repeat: 6},
        {type: "human", id: 1, repeat: 5}
    ],

// 3: max 80
    [
        {type: "human", id: 1, repeat: 7},
        {type: "zombie", id: 1, repeat: 5}
    ],

// 4: max 30
    [
        {type: "pos", x: 8000, y: 6500, repeat: 12}
    ],

// 5: max 240
    [
        {type: "pos", x: 4800, y: 3000, repeat: 4},
        {type: "pos", x: 5200, y: 2800, repeat: 1},
        {type: "pos", x: 5600, y: 2600, repeat: 1},
        {type: "pos", x: 6000, y: 2400, repeat: 1},
        {type: "pos", x: 6400, y: 2200, repeat: 1},
        {type: "pos", x: 6800, y: 2000, repeat: 1},
        {type: "pos", x: 7200, y: 1800, repeat: 1},
        {type: "pos", x: 7600, y: 1600, repeat: 1},
        {type: "pos", x: 8000, y: 1400, repeat: 1},
        {type: "pos", x: 8300, y: 1200, repeat: 1},
        {type: "pos", x: 8300, y: 8000, repeat: 1}
    ],

// 6: max 83160
    [
        {type: "pos", x: 7000, y: 9000, repeat: 8},
        {type: "pos", x: 16000, y: 9000, repeat: 5},
        {type: "pos", x: 16000, y: 5000, repeat: 5},
        {type: "pos", x: 16000, y: 0, repeat: 2},
        {type: "pos", x: 10000, y: 0, repeat: 5},
        {type: "pos", x: 4000, y: 4500, repeat: 3},
        {type: "pos", x: 11000, y: 2000, repeat: 3}
    ],

// 7: max 4160
    [
        {type: "pos", x: 2000, y: 4500, repeat: 2 },
        {type: "pos", x: 16000, y: 4500, repeat: 6 },
        {type: "pos", x: 7250, y: 4500, repeat: 1 },
        {type: "pos", x: 0, y: 5000, repeat: 7 },
        {type: "pos", x: 4000, y: 0, repeat: 6 },
        {type: "pos", x: 0, y: 4000, repeat: 5 },
        {type: "pos", x: 4000, y: 9000, repeat: 3 },
        {type: "pos", x: 8000, y: 7000, repeat: 1 },
        {type: "pos", x: 4000, y: 7000, repeat: 1 },
        {type: "pos", x: 16000, y: 7000, repeat: 1 },
        {type: "pos", x: 0, y: 0, repeat: 1 },
    ],
// 8: max
    [
        {type: "pos", x: 2500, y: 2800, repeat: 3 },
        {type: "human", id: 1, repeat: 5 },
        // {type: "pos", x: 2000, y: 8000, repeat: 3 },
        // {type: "pos", x: 1500, y: 0, repeat: 10 },
    ],

    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
    [{type: "human", id: 1, repeat: 100}],
    [{type: "human", id: 1, repeat: 100}],
    [{type: "human", id: 0, repeat: 100}],
][env].reduce((acc, curr) =>
{
    let r = curr.repeat;
    if(!curr.repeat)
        r = 1;
    else
        delete curr.repeat;

    if(r > 0)
        return acc.concat([].constructor(r).fill(curr));
    return acc;

}, []);


while(true)
{
    let {x, y, humans, zombies} = readInputs();
    let target = {x: 0, y: 0};
    if(moves.length > 0)
    {
        let move = moves.shift();
        console.error(move.type, move.id);
        switch(move.type)
        {
            case "human":
                target = humans.filter(h => h.id === move.id)[0];
                break;
            case "zombie":
                target = zombies.filter(z => z.id === move.id)[0];
                break;
            case "pos":
                target = move;
                break;
            case "none":
                target = {x, y};
                break;
        }
    }

    console.log(target.x, target.y);
}