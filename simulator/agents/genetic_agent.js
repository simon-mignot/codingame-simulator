function readInputs()
{
    let humans = [];
    let zombies = [];

    var inputs = readline().split(' ');
    const x = parseInt(inputs[0]);
    const y = parseInt(inputs[1]);
    const humanCount = parseInt(readline());
    for (let i = 0; i < humanCount; i++) {
        var inputs = readline().split(' ');
        const humanId = parseInt(inputs[0]);
        const humanX = parseInt(inputs[1]);
        const humanY = parseInt(inputs[2]);
        humans.push({id: humanId, x: humanX, y: humanY});
    }
    const zombieCount = parseInt(readline());
    for (let i = 0; i < zombieCount; i++) {
        var inputs = readline().split(' ');
        const zombieId = parseInt(inputs[0]);
        const zombieX = parseInt(inputs[1]);
        const zombieY = parseInt(inputs[2]);
        const zombieXNext = parseInt(inputs[3]);
        const zombieYNext = parseInt(inputs[4]);
        zombies.push({id: zombieId, x: zombieX, y: zombieY, nextX: zombieXNext, nextY: zombieYNext});
    }
    return { x, y, humans, zombies };
}



let sequence;
try
{
    sequence = require('node:worker_threads').workerData.map(d => [d.deltaX, d.deltaY]);
}
catch(e)
{
    sequence =
    [[-169.3361104935984,1611.125470496603],
        [-683.7963474964525,-1466.4059994252993],
        [595.9822514817575,-170.89516060653986],
        [-1014.2670977600621,-764.305079403101],
        [-114.05183457164533,-1631.0172221748126],
        [-1362.4295818414053,-1315.682953649341],
        [152.65271612339015,-612.2566032801539],
        [130.7764535243707,615.2548408615637],
        [-198.73179008858096,-220.71401316678603],
        [1354.6867180384681,-190.38880211337013],
        [1367.5445148147826,-1367.5445148147833],
        [673.6414149514932,-802.814576388689],
        [723.4972976409547,368.64028578851196],
        [909.5673613300183,-367.48906814101],
        [-208.24916125480965,-467.7352743130117],
        [228.69464998734736,1296.991810717078],
        [366.19021217821063,-504.01758749559235],
        [-1265.7229953495446,-269.0377279181802],
        [129.36849886529745,665.5432303775161],
        [737.2402272275608,790.5933514503232],
        [-631.6637629659804,-1010.8733306184597],
        [1644.3767166743205,-534.2903832742844],
        [286.7715019735934,-1070.2458155282875],
        [-504.0548441134653,339.98928531021414],
        [-1351.8362053104986,-118.27034290857183],
        [1029.255583286191,926.7458898070187],
        [165.57308581596308,1575.3226822633449],
        [-714.1890262703594,-274.15148140215484],
        [938.8204704854156,-1079.989409298787],
        [90.2458126353441,68.00509761618146],
        [523.9704433041157,-1234.3970084797227],
        [843.1015493950287,-1349.2445209848738],
        [54.23381787568699,-1553.0533451876747],
        [-55.81819938492689,-531.074692126658],
        [-961.4999999999995,1665.3668514774756],
        [1463.9362299820982,-153.86589792998583],
        [187.31915355157045,352.2960895507118],
        [-354.67435276209767,-796.6116390643481],
        [-1263.5696898357178,-1453.5706515090583],
        [-801.5912408740068,-1025.9900011959521]];
}

function checkIntersection(x1, y1, x2, y2, x3, y3, x4, y4) {
    const denom = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
    const numeA = ((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3));
    const numeB = ((x2 - x1) * (y1 - y3)) - ((y2 - y1) * (x1 - x3));

    if(denom === 0)
        return null;

    const uA = numeA / denom;
    const uB = numeB / denom;

    if(uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1)
        return { x: x1 + (uA * (x2 - x1)), y: y1 + (uA * (y2 - y1)) };
    return null;
}
function segmentRectangleIntersection(seg)
{
    let rectangle = { x: 0, y:0, width: 16000, height: 9000 };
    let rectangleSegments =
    [
        [rectangle.x, rectangle.y, rectangle.width, rectangle.y],
        [rectangle.x, rectangle.y, rectangle.x, rectangle.height],
        [rectangle.width, rectangle.y, rectangle.width, rectangle.height],
        [rectangle.x, rectangle.height, rectangle.width, rectangle.height]
    ];
    for(let i = 0; i < rectangleSegments.length; ++i)
    {
        let intersect = checkIntersection(seg[0], seg[1], seg[2], seg[3],
        rectangleSegments[i][0], rectangleSegments[i][1], rectangleSegments[i][2], rectangleSegments[i][3]);
        if(intersect !== null)
            return intersect;
    }
    return null;
}
while(true)
{
    let { x, y, humans, zombies } = readInputs();
    if(sequence.length > 0)
    {
        const move = sequence.pop();
        let nextPoint = { x: move[0] + x, y: move[1] + y };
        if(nextPoint.x < 0 || nextPoint.x >= 16000 || nextPoint.y < 0 || nextPoint.y >= 9000)
        {
            let tmp = segmentRectangleIntersection([x, y, nextPoint.x, nextPoint.y]);
            if(tmp !== null)
                nextPoint = tmp;
        }
        console.log(Math.abs(Math.trunc(nextPoint.x)), Math.abs(Math.trunc(nextPoint.y)));
    }
    else
        console.log(humans[0].x, humans[0].y);
}