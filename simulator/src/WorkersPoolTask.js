const workerThreads = require("node:worker_threads");

class WorkersPoolTask
{
    maxThread = 10;
    currentRunningThread = 0;
    tasksParams;
    onNewResult;
    constructor(onNewResult, tasksParams)
    {
        this.tasksParams = tasksParams;
        this.onNewResult = onNewResult;
    }
    runTasks()
    {
        return new Promise((resolve, reject) =>
        {
            const onThreadFinished = (workerData, onThreadFinished, result) =>
            {
                this.currentRunningThread--;
                this.onNewResult(workerData, result);
                this._runTasks(onThreadFinished);
                if(this.tasksParams.length === 0 && this.currentRunningThread === 0)
                    resolve();
            };
            this._runTasks(onThreadFinished);
        });
    }

    _runTasks(onThreadFinished)
    {
        while(this.tasksParams.length && this.currentRunningThread < this.maxThread)
        {
            // if(this.tasksParams.length % 10 === 0) console.log(this.tasksParams.length);
            // if(this.tasksParams.length % 1000 === 0) console.log(process.memoryUsage());

            const params = this.tasksParams.pop();
            const w = new workerThreads.Worker(...params);
            w.on("message", onThreadFinished.bind(this, params[1].workerData, onThreadFinished));
            this.currentRunningThread++;
        }
    }
}

module.exports = WorkersPoolTask;