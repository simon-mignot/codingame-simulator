const { durationFormat } = require("./utils");

const fs = require("fs");
const Individual = require("./Individual");
const WorkersPoolTask = require("../WorkersPoolTask");
const GeneticEvolution = require("./GeneticsEvolution");

class Evolution
{
    populationSize;
    population;
    constructor(agent, environment)
    {
        this.agent = agent;
        this.environment = environment;
    }

    async start(populationSize, generation, initialPopulation = [])
    {
        this.populationSize = populationSize;
        this.population = initialPopulation;
        this.initializePopulation();
        let elapsed = 0;
        for(let i = 0; i < generation; ++i)
        {
            let startTime = performance.now();
            console.log("=== SIMULATE ===");
            await this.simulate();
            this.population.sort((l, r) => l.fitness - r.fitness);
            let best = this.population[this.population.length - 1];
            if(i % 10 === 0)
            {
                let json = JSON.stringify(best.genes.map(d => [d.deltaX, d.deltaY])).replace(/],/g, "],\n");
                //console.log(best.genes, json);
                fs.writeFileSync("./DNAs/score_s" + best.score + "-f" + best.fitness + ".json", json);
            }
            let log = ["generation", i+1, "max fitness:", Math.max(...this.population.map(i => i.fitness))];
            //console.dir(results.splice(-10), { depth: null });
            console.log("=== BREED ===");
            if(i < generation - 1)
                this.breed();
            let endTime = performance.now();
            let time = endTime - startTime;
            elapsed += time;
            let meanTime = elapsed / (i + 1);
            let remainingTime = (generation - i - 1) * meanTime;
            let totalTime = elapsed + remainingTime;

            console.log(...log, "Time (step, elapsed, remaining, total):", durationFormat(time), "/", durationFormat(elapsed), "/", durationFormat(remainingTime), "/", durationFormat(totalTime), "-", (elapsed / totalTime * 100).toFixed(3), "%")
        }
        this.population.sort((l, r) => l.fitness - r.fitness);
        console.log("Output")
        //console.dir(this.population.splice(-10), { depth: null });
    }

    async simulate()
    {
        let nextPop = [];
        const computeScores = (result) =>
        {
            let fitness = result.falseScore;
            if(result.status !== "WIN")
                fitness /= 2;
            fitness -= result.rounds * 2;
            return { fitness, score: result.score };
        };
        const onNewResult = (genes, result) =>
        {
            let individual = new Individual(computeScores(result.result), genes);
            nextPop.push(individual);
        };
        let wpt = new WorkersPoolTask(onNewResult, this.population.map(individual =>
        {
            return ["./src/simulator/SimulatorEntry.js", { argv: [this.environment, this.agent], stdout: false, stderr: false, workerData: individual.genes }]
        }));
        await wpt.runTasks();
        this.population = nextPop;
    }

    initializePopulation()
    {
        for(let i = this.population.length; i < this.populationSize; ++i)
            this.population.push(new Individual());
    }

    breed()
    {
        this.population = (new GeneticEvolution(this.population)).breed().map(i => { i.score = 0; i.fitness = 0; return i;});
        this.initializePopulation();
    }
}

module.exports = Evolution;