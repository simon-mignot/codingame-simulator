const { randomIntBetween } = require("./utils");

class Gene
{
    static genesLimits =
    {
        angle: { min: 0, max: 360 },
        distance: { min: 0, max: 2000 }
    };
    static adjustGene(v, prevV, geneLimits)
    {
        if(v < geneLimits.min)
            return geneLimits.min;
        if(v > geneLimits.max)
            return geneLimits.max;
        return v;
    }
    angle;
    distance;
    deltaX;
    deltaY;
    constructor(angle = null, distance = null)
    {
        this.angle = angle === null ? randomIntBetween(Gene.genesLimits.angle.min, Gene.genesLimits.angle.max + 1) : angle;
        this.distance = distance === null ? randomIntBetween(Gene.genesLimits.distance.min, Gene.genesLimits.distance.max + 1) : distance;
        this.calculateDeltas();
    }
    calculateDeltas()
    {
        this.deltaX = this.distance * Math.cos(this.degToRad(this.angle));
        this.deltaY = this.distance * Math.sin(this.degToRad(this.angle));
    }
    degToRad(deg)
    {
        return deg * Math.PI / 180;
    }
}

module.exports = Gene;