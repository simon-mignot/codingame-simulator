const Gene = require("./Gene");

class Individual
{
    static maxSteps = 40;
    genes = [];
    score = 0;
    fitness = 0;
    constructor(scores = { score: 0, fitness: 0 }, genes = [])
    {
        this.score = scores.score;
        this.fitness = scores.fitness;
        this.genes = genes;
        if(this.genes.length === 0)
            this.randomGenes();
    }
    randomGenes()
    {
        for(let i = 0; i < Individual.maxSteps; ++i)
            this.genes.push(new Gene());
    }
}

module.exports = Individual;