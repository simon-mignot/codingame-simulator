module.exports =
{
    randomIntBetween,
    copyObject,
    durationFormat
}

function randomIntBetween(min, max)
{
    return Math.floor(Math.random() * (max - min) + min);
}
function copyObject(obj)
{
    return JSON.parse(JSON.stringify(obj));
}
function durationFormat(milliseconds)
{
    let totalSeconds = milliseconds / 1000;

    let hours   = Math.floor(totalSeconds / 3600)
    let minutes = Math.floor(totalSeconds / 60) % 60
    let seconds = totalSeconds % 60;
    let millis = seconds - Math.trunc(seconds);
    return [hours, minutes, Math.trunc(seconds)].map(v => (""+v).padStart(2, "0")).join(":") + (""+millis.toFixed(3)).substring(1);
}