const { copyObject, randomIntBetween } = require("./utils");
const Gene = require("./Gene");
const Individual = require("./Individual");

class GeneticEvolution
{
    topPerformers_keepCount;
    reproduction_newIndividuals;
    reproduction_selectionThreshold;
    mutation_count;
    mutation_genesSelectionMin;
    mutation_genesSelectionMax;
    mutation_genesDeviation;

    populationCount;
    population;
    finalPopulation;
    constructor(population)
    {
        this.populationCount = population.length;
        this.population = population;


        // ====== Selection parameters
        // ==== 1. Keep top perfomers
        this.topPerformers_keepCount = Math.trunc(this.populationCount * .05); // 5% of total
        // ==== 2. Reproduction
        this.reproduction_newIndividuals = this.populationCount * .25; // 55% of total as there is 2 reproduction steps
        this.reproduction_selectionThreshold = .3; // Reproduce only the bests (%) [0;1]
        // == 2.1 Reproduction between bests
        // == 2.2 Reproduction with randoms
        // ==== 3. Mutation
        this.mutation_count = this.populationCount * .15; // 85% of total (2 steps aswell)
        this.mutation_genesSelectionMin = .05;
        this.mutation_genesSelectionMax = .3;
        // == 3.1 Normal mutation
        this.mutation_genesDeviation = .1; // max value change of genes
    }

    breed()
    {
        let distribution = [];
        for(let i = Math.ceil(this.populationCount * (1 - this.reproduction_selectionThreshold) - 1); i < this.populationCount; ++i)
            distribution = distribution.concat(new Array(i * 2).fill(i));

        this.finalPopulation = [];
        this.population.sort((l, r) => l.fitness - r.fitness);

        this.keepBest();
        this.reproduceWithGeneration(distribution);
        this.reproduceWithRandoms(distribution);
        this.mutate();
        this.mutateAtRandom();
        return this.finalPopulation;
    }

    // Evolution steps
    mutateAtRandom()
    {
        this.logBefore();
        for(let i = Math.ceil(this.populationCount - this.mutation_count - 1); i < this.populationCount; ++i)
        {
            let copy = copyObject(this.population[i]);
            this.finalPopulation.push(this.mutateIndividual(copy, true));
        }
        this.log("mutateAtRandom");
    }

    mutate()
    {
        this.logBefore();
        for(let i = Math.ceil(this.populationCount - this.mutation_count - 1); i < this.populationCount; ++i)
        {
            let copy = copyObject(this.population[i]);
            this.finalPopulation.push(this.mutateIndividual(copy));
        }
        this.log("mutate");
    }

    reproduceWithRandoms(distribution)
    {
        this.logBefore();
        for(let i = 0; i < Math.ceil(this.reproduction_newIndividuals); ++i)
        {
            let r1 = distribution[randomIntBetween(0, distribution.length)];
            this.finalPopulation.push(this.reproduce(this.population[r1], new Individual({ fitness: this.population[r1].fitness }))); // Take the fitness from r1 to have a 50/50 genes selection chance
        }
        this.log("reproduceWithRandoms");
    }

    reproduceWithGeneration(distribution)
    {
        this.logBefore();
        for(let i = 0; i < Math.ceil(this.reproduction_newIndividuals); ++i)
        {
            let r1 = distribution[randomIntBetween(0, distribution.length)];
            let r2 = r1;
            let j = 0;
            while(r2 === r1 || (r1.fitness === r2.fitness && j++ < 10))
                r2 = distribution[randomIntBetween(0, distribution.length)];
            this.finalPopulation.push(this.reproduce(this.population[r1], this.population[r2]));
        }
        this.log("reproduceWithGeneration");
    }

    keepBest()
    {
        this.logBefore();
        // Copy and keep top-performing individuals
        for(let i = this.populationCount - this.topPerformers_keepCount; i < this.populationCount; ++i)
            this.finalPopulation.push(copyObject(this.population[i]));
        this.log("keepBest");
    }

    // Methods
    reproduce(father, mother)
    {
        const totalFitness = father.fitness + mother.fitness;
        const mid = father.fitness / totalFitness;
        let individual = new Individual();
        individual.genes = [];
        for(let i = 0; i < Individual.maxSteps; ++i)
            individual.genes.push(Math.random() > mid ? copyObject(mother.genes[i]) : copyObject(father.genes[i]));
        return individual;
    }

    mutateIndividual(individual, random = false)
    {
        let geneSelectionChance = randomIntBetween(this.mutation_genesSelectionMin * 100, this.mutation_genesSelectionMax * 100) / 100;
        for(let i = 0; i < Individual.maxSteps; ++i)
        {
            let selectGene = Math.random() < geneSelectionChance;
            if(!selectGene)
                continue;
            let gene = new Gene();
            if(!random)
            {
                Object.entries(Gene.genesLimits).forEach(entry =>
                {
                    let key = entry[0], value = entry[1];
                    let maxGeneDeviation = (Gene.genesLimits[key].max - Gene.genesLimits[key].min) * this.mutation_genesDeviation / 2;
                    gene[key] = Gene.adjustGene(individual.genes[i][key] + randomIntBetween(-maxGeneDeviation, maxGeneDeviation), individual.genes[i][key], Gene.genesLimits[key]);
                });
            }
            gene.calculateDeltas();
            individual.genes[i] = gene;
        }
        return individual;
    }

    // Debug
    logBefore()
    {
        this.__populationCount = this.finalPopulation.length;
    }
    log(name)
    {
        console.log(name.padEnd(30), "generated", this.finalPopulation.length - this.__populationCount);
    }
}

module.exports = GeneticEvolution;