const Entity = require("./Entity");

class EntityZombie extends Entity
{
    nextX;
    nextY;
    constructor(x, y, id)
    {
        super(x, y, id)
        this.nextX = x;
        this.nextY = y;
    }
}

module.exports = EntityZombie;