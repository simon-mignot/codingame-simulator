"use strict";

//console.error(process.argv);

const environment = require(process.argv[2]);
const Game = require("./Game");
let game = new Game(environment);

// setup node env
console.log = (...data) => game.nextStep(data.join(" ").split(" "));
console.error = (...data) => game.addLog(...data);
global.readline = () => game.popEvent();

// start the agent
delete require.cache[require.resolve(process.argv[3])];
require(process.argv[3]);
