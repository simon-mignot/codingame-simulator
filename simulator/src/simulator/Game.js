const { parentPort, isMainThread } = require('node:worker_threads');
const { readJsonEntity, vectorTo_CG, fibonacci, distance } = require("./utils");
const Entity = require("./Entity");
const EntityZombie = require("./EntityZombie");

function log(...data)
{
    if(!isMainThread)
        return;
    console.error("[DEBUG]", ...data);
}
function logGame(...data)
{
    if(!isMainThread)
        return;
    console.error("[GAME]", ...data);
}


class Game
{
    // Constants
    static WIDTH = 16000;
    static HEIGHT = 9000;
    static ZOMBIE_SPEED = 400;
    static ASH_SPEED = 1000;
    static ASH_RANGE = 2000;

    ash;
    humans = [];
    zombies = [];
    gameEvents = [];
    roundsCounter = 0;
    score = 0;

    history = [];
    result = {};
    frameLog = [];

    timeoutId = null;

    addToHistory(roundScore, zombiesKilled, humansEaten, ashTarget)
    {
        let step =
        {
            round: this.roundsCounter,
            ash: this.ash,
            humans: this.humans,
            zombies: this.zombies,
            totalScore: this.score,
            logs: this.frameLog,
            roundScore,
            zombiesKilled,
            humansEaten,
            ashTarget
        }
        this.history.push(JSON.parse(JSON.stringify(step)));
        this.frameLog = [];
    }

    constructor(environment)
    {
        this.ash = new Entity(environment.ash.x, environment.ash.y, "ash");
        this.humans = environment.humans.map(readJsonEntity.bind(null, Entity));
        this.zombies = environment.zombies.map(readJsonEntity.bind(null, EntityZombie));

        this.result =
        {
            totalHumans: this.humans.length,
            totalZombies: this.zombies.length,
            agent: process.argv[3].split("/").pop(),
            environment: process.argv[2].split("/").pop()
        };

        this.displayStartRound();
        this.generateGameEvents();
        this.displayRound();
        this.addToHistory(0, 0, 0, null);
        this.roundsCounter++;

        this.setTimeout(1000);
    }

    computeZombiesPositions()
    {
        this.zombies = this.zombies.map(z =>
        {
            const target = [...this.humans, this.ash].map(h => ({...h, distance: distance(h, z)}))
                                      .sort((l, r) => l.distance - r.distance)[0];

            if(target.distance > Game.ZOMBIE_SPEED)
            {
                const pos = vectorTo_CG(z, target, Game.ZOMBIE_SPEED);
                z.nextX = pos.x;
                z.nextY = pos.y;
            }
            else
            {
                z.nextX = target.x;
                z.nextY = target.y;
            }
            return z;
        });
    }
    generateGameEvents()
    {
        this.computeZombiesPositions();
        this.gameEvents.push(this.ash.x + " " + this.ash.y);
        this.gameEvents.push(this.humans.length);
        this.humans.forEach(human => this.gameEvents.push(human.id + " " + Math.trunc(human.x) + " " + Math.trunc(human.y)));
        this.gameEvents.push(this.zombies.length);
        this.zombies.forEach(zombie => this.gameEvents.push(zombie.id + " " + Math.trunc(zombie.x) + " " + Math.trunc(zombie.y) + " " + Math.trunc(zombie.nextX) + " " + Math.trunc(zombie.nextY)));
    }
    popEvent()
    {
        return this.gameEvents.shift();
    }

    displayStartRound()
    {
        if(!isMainThread)
            return;
        if(this.roundsCounter === 0)
            log("==== Start");
        else
            log("==== Round", this.roundsCounter);
    }
    displayRound()
    {
        if(!isMainThread)
            return;
        log("Ash:", this.ash);
        log("Humans:", this.humans);
        log("Zombies:", this.zombies);
        log(" ")
    }
    nextStep(data)
    {
        clearTimeout(this.timeoutId);

        this.displayStartRound();

        // Zombies moves
        this.zombies.map(z =>
        {
            z.x = z.nextX;
            z.y = z.nextY;
            return z;
        });

        // Ash moves
        let ashTarget = new Entity(+data[0], +data[1]);
        if(distance(this.ash, ashTarget) > Game.ASH_SPEED)
        {
            const pos = vectorTo_CG(this.ash, ashTarget, Game.ASH_SPEED);
            this.ash.x = pos.x;
            this.ash.y = pos.y;
        }
        else
        {
            this.ash.x = ashTarget.x;
            this.ash.y = ashTarget.y;
        }

        // Ash shoots
        let roundScore = 0;
        let humansValue = this.humans.length * this.humans.length * 10;
        let zombiesKilledCount = 0;
        this.zombies = this.zombies.filter(z =>
        {
            let killed = distance(this.ash, z) <= Game.ASH_RANGE;
            if(killed)
            {
                let score = humansValue * fibonacci(zombiesKilledCount++ + 2);
                roundScore += score;
                this.score += score;
                logGame("Human alive:", this.humans.length, "{SCORE} Zombie", zombiesKilledCount, "=", score, "{TOTAL ROUND SCORE}", roundScore, "{TOTAL SCORE}", this.score);
            }
            return !killed;
        })

        // Zombies eats
        const humansBefore = this.humans.length;
        this.humans = this.humans.filter(h =>
        {
            if(this.zombies.filter(z => z.x === h.x && z.y === h.y).length > 0)
            {
                logGame("Zombies have eaten human", h.id);
                return false;
            }
            return true;
        });
        const humansEaten = humansBefore - this.humans.length;

        this.generateGameEvents();
        this.displayRound();
        this.addToHistory(roundScore, zombiesKilledCount, humansEaten, ashTarget);

        this.checkGameEnd();
        this.roundsCounter++;

        this.setTimeout(100)
    }
    checkGameEnd(byTimeout)
    {
        if(this.zombies.length > 0 && this.humans.length > 0 && this.roundsCounter < 100 && byTimeout === undefined)
            return;
        this.result = { agent: this.result.agent, environment: this.result.environment, status: "", score: this.score, falseScore: this.score, rounds: this.roundsCounter,
            totalHumans: this.result.totalHumans, humansLeft: this.humans.length,
            totalZombies: this.result.totalZombies, zombiesLeft: this.zombies.length }
        if(byTimeout)
        {
            log("TIMEOUT");
            this.result.status = "TIMEOUT";
            this.result.score = 0;
        }
        else if(this.zombies.length === 0)
        {
            log("SUCCESS! Score:", this.score, "- Humans alive:", this.humans.length, "- Rounds:", this.roundsCounter);
            this.result.status = "WIN";
        }
        else if(this.roundsCounter >= 100)
        {
            log("DIED_OF_OLD_AGE");
            this.result.status = "DIED_OF_OLD_AGE";
            this.result.score = 0;
        }
        else if(this.humans.length === 0)
        {
            log("LOST");
            this.result.status = "LOST";
            this.result.score = 0;
        }
        if(!isMainThread)
            parentPort.postMessage({ history: this.history, result: this.result });
        process.exit(0);
    }

    setTimeout(msTime)
    {
        this.timeoutId = setTimeout(this.checkGameEnd.bind(this, true), msTime);
    }

    addLog(...output)
    {
        let place = new Error().stack.split("\n")[3];
        place = place.substring(place.indexOf("(") + 1, place.indexOf(")"));
        this.frameLog.push({ place, output });
    }
}

module.exports = Game;