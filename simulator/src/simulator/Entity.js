class Entity
{
    id;
    x;
    y;
    constructor(x, y, id)
    {
        this.id = id;
        this.x = x;
        this.y = y;
    }
}

module.exports = Entity;