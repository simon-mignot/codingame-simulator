module.exports =
{
    fibonacci,
    readJsonEntity,
    distance,
    vectorTo,
    vectorTo_CG
}

function fibonacci(n) {
    return n < 1 ? 0
                 : n <= 2 ? 1
                          : fibonacci(n - 1) + fibonacci(n - 2)
}
function readJsonEntity(entityClass, entity, id)
{
    return new entityClass(entity.x, entity.y, id);
}

function distance(first, second)
{
    return Math.sqrt(Math.pow((first.x - second.x), 2) + Math.pow((first.y - second.y), 2));
}
function _vectorTo(from, to, dist)
{
    let length = distance(from, to);
    let x = (to.x - from.x) / length * dist + from.x;
    let y = (to.y - from.y) / length * dist + from.y;
    return { x, y };
}
function vectorTo_CG(from, to, dist)
{
    let v = _vectorTo(from, to, dist)
    return {
        x: Math.floor(v.x),
        y: Math.floor(v.y)
    };
}
function vectorTo(from, to, dist)
{
    let v = _vectorTo(from, to, dist)
    return {
        x: v.x > from.x ? Math.floor(v.x) : Math.ceil(v.x),
        y: v.y > from.y ? Math.floor(v.y) : Math.ceil(v.y)
    };
}