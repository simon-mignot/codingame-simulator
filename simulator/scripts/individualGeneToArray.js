const data =
[
    {
        angle: 220,
        distance: 753,
        deltaX: -576.8314656685906,
        deltaY: -484.01907009396405
    },
    {
        angle: 228,
        distance: 797,
        deltaX: -533.2970932680101,
        deltaY: -592.286425905483
    },
    {
        angle: 170,
        distance: 607,
        deltaX: -597.7783060784103,
        deltaY: 105.40444384382668
    },
    {
        angle: 354,
        distance: 958,
        deltaX: 952.7519757628058,
        deltaY: -100.13826781041197
    },
    {
        angle: 83,
        distance: 64,
        deltaX: 7.799637977929439,
        deltaY: 63.52295370504461
    },
    {
        angle: 310,
        distance: 1803,
        deltaX: 1158.9460602648303,
        deltaY: -1381.1781309435175
    },
    {
        angle: 244,
        distance: 1946,
        deltaX: -853.0702516515453,
        deltaY: -1749.0532140981786
    },
    {
        angle: 258,
        distance: 1170,
        deltaX: -243.25667825677894,
        deltaY: -1144.4326928585526
    },
    {
        angle: 50,
        distance: 283,
        deltaX: 181.90889354129064,
        deltaY: 216.79057740267078
    },
    {
        angle: 109,
        distance: 117,
        deltaX: -38.091474071487305,
        deltaY: 110.62567334512008
    },
    {
        angle: 267,
        distance: 330,
        deltaX: -17.27086556017162,
        deltaY: -329.5477464690094
    },
    {
        angle: 292,
        distance: 1508,
        deltaX: 564.9067428711952,
        deltaY: -1398.1932526867154
    },
    {
        angle: 219,
        distance: 716,
        deltaX: -556.436508403191,
        deltaY: -450.5933999916837
    },
    {
        angle: 296,
        distance: 1623,
        deltaX: 711.4763712386726,
        deltaY: -1458.742737143548
    },
    {
        angle: 128,
        distance: 1600,
        deltaX: -985.0583605210533,
        deltaY: 1260.8172057707552
    },
    {
        angle: 334,
        distance: 1310,
        deltaX: 1177.420200651909,
        deltaY: -574.2662022936909
    },
    {
        angle: 311,
        distance: 993,
        deltaX: 651.4666157875735,
        deltaY: -749.4266131612127
    },
    {
        angle: 307,
        distance: 1877,
        deltaX: 1129.606798456394,
        deltaY: -1499.038852358769
    },
    {
        angle: 5,
        distance: 1787,
        deltaX: 1780.1999254899492,
        deltaY: 155.74731229006514
    },
    {
        angle: 5,
        distance: 1721,
        deltaX: 1714.4510754158941,
        deltaY: 149.9950332687197
    },
    {
        angle: 309,
        distance: 1891,
        deltaX: 1190.0448594752427,
        deltaY: -1469.5830131151317
    },
    {
        angle: 320,
        distance: 1934,
        deltaX: 1481.529952992103,
        deltaY: -1243.1512371337676
    },
    {
        angle: 183,
        distance: 1208,
        deltaX: -1206.3444779835252,
        deltaY: -63.22183514147583
    },
    {
        angle: 345,
        distance: 1319,
        deltaX: 1274.0561648752812,
        deltaY: -341.3823204902248
    },
    {
        angle: 17,
        distance: 1234,
        deltaX: 1180.0800688583856,
        deltaY: 360.7866836278572
    },
    {
        angle: 124,
        distance: 688,
        deltaX: -384.7247175878737,
        deltaY: 570.3778499178687
    },
    {
        angle: 347,
        distance: 114,
        deltaX: 111.0781873855168,
        deltaY: -25.64442019520065
    },
    {
        angle: 142,
        distance: 1305,
        deltaX: -1028.3540334567722,
        deltaY: 803.4382252999842
    },
    {
        angle: 292,
        distance: 86,
        deltaX: 32.21616703376843,
        deltaY: -79.73781149274372
    },
    {
        angle: 49,
        distance: 1274,
        deltaX: 835.8192029339062,
        deltaY: 961.5000052038115
    },
    {
        angle: 351,
        distance: 178,
        deltaX: 175.8085246259345,
        deltaY: -27.84533477716114
    },
    {
        angle: 205,
        distance: 1249,
        deltaX: -1131.9784260087758,
        deltaY: -527.8502089141334
    },
    {
        angle: 106,
        distance: 864,
        deltaX: -238.1506754258872,
        deltaY: 830.5301052907075
    },
    {
        angle: 173,
        distance: 356,
        deltaX: -353.34642998431065,
        deltaY: 43.385486252232525
    },
    {
        angle: 181,
        distance: 140,
        deltaX: -139.97867732189476,
        deltaY: -2.443336901219647
    },
    {
        angle: 161,
        distance: 1197,
        deltaX: -1131.7857349923822,
        deltaY: 389.7050808852169
    },
    {
        angle: 170,
        distance: 702,
        deltaX: -691.33504261457,
        deltaY: 121.90102072218505
    },
    {
        angle: 284,
        distance: 162,
        deltaX: 39.19134708714613,
        deltaY: -157.18790765671145
    },
    {
        angle: 196,
        distance: 273,
        deltaX: -262.4244429911611,
        deltaY: -75.24899813804073
    },
    {
        angle: 172,
        distance: 1638,
        deltaX: -1622.0590965986921,
        deltaY: 227.9655393725877
    }
];

console.log(data.map(d => [d.deltaX, d.deltaY]));