"use strict";

/*

compare on 17.json:

  '_58k.js': 11200,
  'first.js': 4190,
  'genetic_agent.js': 2600,
  'gotoFirstHuman.js': 2600,
  'gotoFirstZombie.js': 4190,
  'gotoNearestSavableHumanOrFirstHuman.js': 2600,
  'gotoNearestZombie.js': 0

 */

const path = require("node:path");
const Genetics = require("./src/evolution/Evolution.js");

const environment = path.join(__dirname, "environments", "10.json");
const agent = path.join(__dirname, "agents", "genetic_agent.js");
const populationSize = 200;
const generation = 500;

(new Genetics(agent, environment)).start(populationSize, generation).then(() => { });




