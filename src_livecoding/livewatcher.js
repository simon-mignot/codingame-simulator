const fs = require("fs");
const path = require("path");
const axios = require("axios");

const fileToWatch = typeof (process.argv[2]) && fs.existsSync(process.argv[2])
                    ? process.argv[2]
                    : path.join(__dirname, "readytosend.js");

let lastUpdate = performance.now();
let serverReady = true;
fs.watch("./readytosend.js", (eventType, filename) =>
{
    if(eventType === "change" && serverReady && performance.now() - lastUpdate > 100)
    {
        serverReady = false;
        lastUpdate = performance.now();
        const s = fs.readFileSync(fileToWatch, "utf-8");
        const startTime = performance.now();
        axios.post("http://localhost:3000/simulate",
        {
            "environments": ["01.json", "02.json", "03.json", "04.json", "05.json", "06.json", "07.json", "08.json", "09.json", "10.json", "11.json", "12.json", "13.json", "14.json", "15.json", "16.json", "17.json", "18.json", "19.json", "20.json", "21.json"],
            "code": s,
        })
             .then(result =>
             {
                 let now = performance.now();
                 console.log(now, result.status, result.statusText, "\t", Math.round(now - startTime), "ms");
             })
             .catch(err => console.log(performance.now(), err))
             .finally(() => serverReady = true);
    }
});