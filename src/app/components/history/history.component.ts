import { Component, Input, OnInit } from "@angular/core";
import { SimulationService } from "../../services/simulation.service";
import { HistoryObserverValue, SSETestResult } from "../../types/simulator.types";
import { HttpClient } from "@angular/common/http";

@Component({
               selector: "app-history",
               templateUrl: "./history.component.html",
               styleUrls: ["./history.component.scss"]
           })
export class HistoryComponent implements OnInit
{
    environmentName: string = "";
    history: SSETestResult | undefined;

    constructor(private simulationService: SimulationService, private http: HttpClient) { }

    ngOnInit(): void
    {
        this.simulationService.getSSEListener().subscribe(this.updateEnvironment.bind(this));
        this.simulationService.onEnvironmentChanged().subscribe(this.updateEnvironment.bind(this));
    }
    updateEnvironment(data: HistoryObserverValue)
    {
        this.history = data.environments[data.name];
    }

    isString(v: any): boolean
    {
        return typeof(v) === "string";
    }

    openFileInIDE(place: string)
    {
        let split = place.split(":");
        let url = "http://localhost:63342/api/file?file=" + split[0] + "&line=" + split[1] + "&column=" + split[2];
        this.http.get(url).subscribe();
    }
}
