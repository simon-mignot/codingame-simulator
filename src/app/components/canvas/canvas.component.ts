import { Component, HostListener, Input, OnInit, SimpleChanges } from "@angular/core";
import { HistoryFrame } from "../../types/simulator.types";



@Component({
    selector: 'app-canvas',
    templateUrl: './canvas.component.html',
    styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit
{
    gameSurfaceWidth = 16000;
    gameSurfaceHeight = 9000;
    targetWidthRatio = .75;

    posX: number = 0;
    posY: number = 0;

    targetWidth!: number;
    ratio!: number;
    width!: number;
    height!: number;

    frameIndicatorWidth: number = 0;

    @Input() frame: HistoryFrame | undefined;
    @Input() currentFrameId: number = 0;
    @Input() totalFrame: number = 0;

    constructor()
    {
        this.onResize();
    }

    ngOnInit(): void
    {
    }

    @HostListener('window:resize', ['$event'])
    onResize()
    {
        this.targetWidth = window.innerWidth * this.targetWidthRatio;
        this.ratio = (this.gameSurfaceWidth / this.targetWidth);
        this.height = this.gameSurfaceHeight / this.ratio;
        this.width = this.targetWidth;

        if(this.height > window.innerHeight * .5)
        {
            this.height = window.innerHeight * .5;
            this.ratio = (this.gameSurfaceHeight / this.height);
            this.width = this.gameSurfaceWidth / this.ratio;
        }
    }

    onMouseMove($event: any)
    {
        console.log($event);
        this.posX = Math.trunc($event.x / this.width * this.gameSurfaceWidth);
        this.posY = Math.trunc($event.y / this.height * this.gameSurfaceHeight);
    }



    ngOnChanges(changes: SimpleChanges)
    {
        if(changes["totalFrame"])
        {
            let count = changes["totalFrame"].currentValue;
            this.frameIndicatorWidth = this.width / count;
        }
    }

    identify(index: any, item: any)
    {
        return item.id;
    }
}

