import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { SimulationService } from "../../services/simulation.service";
import { HistoryObserverValue, isSSETestResult, SSETestResult } from "../../types/simulator.types";

@Component({
               selector: "app-tests-list",
               templateUrl: "./tests-list.component.html",
               styleUrls: ["./tests-list.component.scss"]
           })
export class TestsListComponent implements OnInit
{
    environments: Environment[] = [];
    totalScore: number = 0;
    totalScoreDiff: number = 0;

    environmentName: string = "";

    submissionValid: boolean = false;

    constructor(private simulationService: SimulationService)
    {

    }

    ngOnInit(): void
    {
        this.simulationService.getEnvironments()
            .subscribe((data: string[]) =>
                       {
                           this.environments = data.map(name => (new Environment(name)))
                           this.simulationService.getSSEListener().subscribe(this.onServerSentEvent.bind(this));
                       });

        this.simulationService.onEnvironmentChanged().subscribe((data) => this.environmentName = data.name);
    }

    onServerSentEvent(data: HistoryObserverValue)
    {
        let prevScore = this.totalScore;
        this.totalScore = 0;
        this.submissionValid = true;
        function getStatus(gameStatus: string)
        {
            if(gameStatus === "WIN" || gameStatus === "ERROR")
                return gameStatus;
            return "LOST";
        }
        if(this.environments)
        {
            this.environments = this.environments.map(env =>
                                                      {
                                                          let tmp = data.environments[env.name];
                                                          if(isSSETestResult(tmp))
                                                          {
                                                              env.scoreDiff = tmp.result.score - env.score;
                                                              env.score = tmp.result.score;
                                                              env.steps = tmp.history.length;
                                                              env.humans = {left: tmp.history[tmp.history.length - 1].humans.length, total: tmp.history[0].humans.length};
                                                              env.zombies = {left: tmp.history[tmp.history.length - 1].zombies.length, total: tmp.history[0].zombies.length};
                                                              env.status = getStatus(tmp.result.status);
                                                              if(env.status !== "WIN")
                                                                  this.submissionValid = false;
                                                              this.totalScore += env.score;
                                                          }
                                                          else
                                                          {
                                                              env = new Environment(env.name);
                                                              env.status = "ERROR";
                                                              this.submissionValid = false;
                                                          }
                                                          return env;
                                                      });
        }
        this.totalScoreDiff = this.totalScore - prevScore;
    }

    changeEnvironment(name: string)
    {
        this.simulationService.setEnvironment(name);
    }
}


class Environment
{
    name = "";
    steps = -1;
    humans = { left: -1, total: -1 };
    zombies = { left: -1, total: -1 };
    logs = [];
    score = -1;
    scoreDiff = 0;
    status = "";
    constructor(name: string)
    {
        this.name = name;
    }
}

