import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { HistoryFrame, HistoryObserverValue, SSETestResult } from "../../types/simulator.types";
import { SimulationService } from "../../services/simulation.service";

@Component({
               selector: "app-player",
               templateUrl: "./player.component.html",
               styleUrls: ["./player.component.scss"]
           })
export class PlayerComponent implements OnInit
{
    currentHistory: HistoryFrame[] | undefined;
    currentFrame: HistoryFrame | undefined;
    frameId = 0;
    intervalId: number | undefined;

    playerStatus: PlayerStatus = "";

    readonly loopTypes: { [k in LoopType]: string } =
    {
        ["NONE"]: "No loop",
        ["LOOP_CURRENT_ENV"]: "Loop current",
        ["LOOP_THROUGH_ENVS"]: "Loop all"
    };
    selectedLoopType: LoopType = "LOOP_CURRENT_ENV";

    constructor(private simulationService: SimulationService)
    {

    }

    ngOnInit(): void
    {
        this.setupEnvironment();
        this.setFrameId(0);
        this.play();
        this.simulationService.getSSEListener().subscribe(this.updateEnvironment.bind(this));
        this.simulationService.onEnvironmentChanged().subscribe(this.updateEnvironment.bind(this));
    }

    updateEnvironment(data: HistoryObserverValue)
    {
        if(!data.environments.hasOwnProperty(data.name))
            return;
        this.currentHistory = data.environments[data.name].history;
        this.setupEnvironment();
        this.setFrameId(0);
        this.play();
    }

    setFrameId(id: number)
    {
        if(!this.currentHistory)
        {
            this.frameId = 0;
            return;
        }

        this.frameId = id;
        if(this.frameId < 0)
            this.frameId = 0;
        else if(this.frameId >= this.currentHistory.length - 1)
        {
            switch(this.selectedLoopType)
            {
                case "NONE":
                    // minus 2 to display the last "real" frame, the very last frame is a copy to be able to view it when on other loop type.
                    this.frameId = this.currentHistory.length - 2;
                    this.stop();
                    break;
                case "LOOP_CURRENT_ENV":
                    if(this.frameId >= this.currentHistory.length - 1)
                        this.frameId = 0;
                    break;
                case "LOOP_THROUGH_ENVS":
                    if(this.frameId >= this.currentHistory.length - 1)
                        this.simulationService.nextEnvironment();
                    break;
            }
        }
        this.currentFrame = this.currentHistory[this.frameId];
    }

    playStop()
    {
        if(this.intervalId === undefined)
            this.play();
        else
            this.stop();
    }

    play()
    {
        if(this.intervalId === undefined)
        {
            this.playerStatus = "PLAYING";
            if(this.currentHistory && this.frameId === this.currentHistory.length - 2) // see other comment "minus 2"
                this.setFrameId(0);
            this.setFrameId(++this.frameId)
            this.intervalId = setInterval(() => this.setFrameId(++this.frameId), 200);
        }
    }

    stop()
    {
        if(this.intervalId === undefined)
            return;
        this.playerStatus = "STOPPED";
        clearInterval(this.intervalId);
        delete this.intervalId;
    }

    firstStep(): void
    {
        this.stop();
        this.selectedLoopType = "NONE";
        this.setFrameId(0);
    }

    previousStep(): void
    {
        this.stop();
        this.selectedLoopType = "NONE";
        this.setFrameId(this.frameId - 1);
    }

    nextStep(): void
    {
        this.stop();
        this.selectedLoopType = "NONE";
        this.setFrameId(this.frameId + 1);
    }

    lastStep(): void
    {
        this.stop();
        this.selectedLoopType = "NONE";
        this.setFrameId(this.currentHistory ? this.currentHistory.length - 1 : 0);
    }

    private setupEnvironmentAshNextPosition()
    {
        if(!this.currentHistory)
            return;
        let lastFrame = this.currentHistory.length - 1;
        this.currentHistory = this.currentHistory.map((frame, i) =>
                                                      {
                                                          frame.ashTarget = { x: frame.ash.x, y: frame.ash.y };
                                                          if(i < lastFrame)
                                                          {
                                                              // @ts-ignore
                                                              frame.ashTarget.x = this.currentHistory[i + 1].ash.x;
                                                              // @ts-ignore
                                                              frame.ashTarget.y = this.currentHistory[i + 1].ash.y;
                                                          }
                                                          return frame;
                                                      });
    }

    private setupEnvironmentScoresData()
    {
        if(!this.currentHistory)
            return;
        this.currentHistory = this.currentHistory.map((d, i) =>
        {
            if(!this.currentHistory)
                return d;
            if(d.roundScore > 0)
            {
                let s =
                {
                    score: d.roundScore,
                    zombiesKilled: d.zombiesKilled,
                    id: i,
                    x: d.ash.x,
                    y: d.ash.y
                };
                if(!d.scoreData)
                    d.scoreData = [];
                if(i > 0)
                {
                    if(!this.currentHistory[i - 1].scoreData)
                        this.currentHistory[i - 1].scoreData = [];
                    // @ts-ignore
                    this.currentHistory[i - 1].scoreData.push({opacity: 0, ...s});
                }
                if(i + 1 < this.currentHistory.length - 1)
                {
                    if(!this.currentHistory[i + 1].scoreData)
                        this.currentHistory[i + 1].scoreData = [];
                    // @ts-ignore
                    this.currentHistory[i + 1].scoreData.push({opacity: 0, ...s});
                }
                d.scoreData.push({opacity: 1, ...s});
            }
            return d;
        });
    }
    private setupEnvironment()
    {
        if(!this.currentHistory)
            return;

        this.setupEnvironmentAshNextPosition();
        this.setupEnvironmentScoresData();

        this.currentHistory.push(this.currentHistory[this.currentHistory.length - 1]);
    }

    onLoopTypeCheckboxChange(type: any)
    {
        this.play();
    }
}


type PlayerStatus = "PLAYING" | "STOPPED" | "";
type LoopType = "NONE" | "LOOP_CURRENT_ENV" | "LOOP_THROUGH_ENVS";
