import { Component } from "@angular/core";

@Component({
               selector: "app-root",
               templateUrl: "./app.component.html",
               styleUrls: ["./app.component.scss"]
           })
export class AppComponent
{
    title = "code-vs-zombies";
    environmentName: string = "";

    changeEnvironmentName($event: string)
    {
        this.environmentName = $event;
    }
}
