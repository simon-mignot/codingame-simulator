import { Injectable, NgZone } from '@angular/core';
import { Observable, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { HistoryFrame, HistoryObserverValue, SSETestResult } from "../types/simulator.types";

@Injectable({
    providedIn: 'root'
})
export class SimulationService
{
    private readonly environments$: Observable<string[]>;
    private readonly environmentChanged$: Subject<HistoryObserverValue>;
    private readonly sseListener$: Observable<HistoryObserverValue>;

    private currentEnvironmentName: string = "";
    private allHistories: { [p: string]: SSETestResult } = {};



    constructor(private http: HttpClient, private zone: NgZone)
    {
        this.environments$ = this.http.get<string[]>("http://localhost:3000/environments");
        this.environmentChanged$ = new Subject<{ name: string, environments: { [p: string]: SSETestResult }}>();

        this.sseListener$ = new Observable((observer) =>
        {
            const eventSource = new EventSource("http://localhost:3000/sse");
            eventSource.onmessage = (event) =>
            {
                this.zone.run(() =>
                {
                    this.allHistories = JSON.parse(event.data).data;
                    console.log(this.allHistories)
                    observer.next({ name: this.currentEnvironmentName, environments: this.allHistories });
                    if(this.currentEnvironmentName.length === 0)
                        this.firstEnvironment();
                })
            }

            eventSource.onerror = (error) =>
            {
                this.zone.run(() =>
                {
                    observer.error(error);
                })
            }
            return () => eventSource.close();
        });

        // TODO: keep last code
        this.http.post("http://localhost:3000/simulate", {
            "environments": ["01.json","02.json","03.json","04.json","05.json","06.json","07.json","08.json","09.json","10.json","11.json","12.json","13.json","14.json","15.json","16.json","17.json","18.json","19.json","20.json","21.json"],
            // "code": "function readInputs(){let t=[],e=[];var i=readline().split(\" \");let l=parseInt(i[0]),n=parseInt(i[1]),a=parseInt(readline());for(let r=0;r<a;r++){var i=readline().split(\" \");let s=parseInt(i[0]),d=parseInt(i[1]),$=parseInt(i[2]);t.push({id:s,x:d,y:$})}let c=parseInt(readline());for(let o=0;o<c;o++){var i=readline().split(\" \");let h=parseInt(i[0]),y=parseInt(i[1]),p=parseInt(i[2]),u=parseInt(i[3]),I=parseInt(i[4]);e.push({id:h,x:y,y:p,nextX:u,nextY:I})}return{x:l,y:n,humans:t,zombies:e}}function distance(t,e,i,l){return Math.sqrt(Math.pow(t-i,2)+Math.pow(e-l,2))}for(;;){function t(t,e){return t.canBeKilledIn>t.willKillIn&&e.canBeKilledIn>e.willKillIn?t.canBeKilledIn-t.willKillIn-(e.canBeKilledIn-e.willKillIn):(console.error(\"sorted by distance\"),t.distanceFromPlayer-e.distanceFromPlayer)}let{x:e,y:i,humans:l,zombies:n}=readInputs();if((void 0===this.target||\"zombie\"===this.target.type&&0===n.filter(t=>t.id===this.target.id).length||\"human\"===this.target.type&&0===l.filter(t=>t.id===this.target.id).length)&&(this.target=null),null===this.target){let a=n.map(t=>(t.closestHuman=l.map(e=>({...e,distance:distance(e.x,e.y,t.x,t.y)})).sort((t,e)=>t.distance-e.distance)[0].distance,t.distanceFromPlayer=distance(t.x,t.y,e,i),t.willKillIn=t.closestHuman\/400,t.canBeKilledIn=(t.distanceFromPlayer-2e3)\/1e3,t)).filter(t=>t.canBeKilledIn<t.willKillIn).sort((t,e)=>t.willKillIn-e.willKillIn),r=l.map(t=>{let l=n.map(e=>({...e,distance:distance(t.x,t.y,e.x,e.y)})).sort((t,e)=>t.distance-e.distance)[0];return t.closestZombie=l.distance,t.deadIn=t.closestZombie\/400,t.savedIn=(distance(t.x,t.y,e,i)-2e3)\/1e3,t}).filter(t=>t.savedIn<t.deadIn);console.error(r),a.length>0?this.target={type:\"zombie\",id:a[0].id}:r.length>0?this.target={type:\"human\",id:r[0].id}:this.target={type:\"human\",id:l[0].id}}let s=null;\"zombie\"===this.target.type&&(s=n.filter(t=>t.id===this.target.id)[0]),\"human\"===this.target.type&&(s=l.filter(t=>t.id===this.target.id)[0]),console.error(s),console.log(s.x,s.y)}"
            "code": "function readInputs(){let r=[],e=[];var t=readline().split(\" \");let l=parseInt(t[0]),o=parseInt(t[1]),n=parseInt(readline());for(let a=0;a<n;a++){var t=readline().split(\" \");let p=parseInt(t[0]),s=parseInt(t[1]),$=parseInt(t[2]);r.push({id:p,x:s,y:$})}let i=parseInt(readline());for(let u=0;u<i;u++){var t=readline().split(\" \");let x=parseInt(t[0]);parseInt(t[1]),parseInt(t[2]);let d=parseInt(t[3]),f=parseInt(t[4]);e.push({id:x,x:d,y:f,nextX:d,nextY:f})}return{x:l,y:o,humans:r,zombies:e}}for(;;){let{x:r,y:e,humans:t,zombies:l}=readInputs();console.error(\"Hello\"),console.error(r,e),console.error({x:r,y:e}),console.error({x:r,y:e},{x:r,y:e,a:\"a\"}),console.log(l[0].nextX,l[0].nextY)}"
        }).subscribe(() => {});
    }

    getEnvironments()
    {
        return this.environments$;
    }
    getSSEListener()
    {
        return this.sseListener$;
    }
    onEnvironmentChanged()
    {
        return this.environmentChanged$;
    }


    firstEnvironment()
    {
        let keys = Object.keys(this.allHistories).sort((l, r) => l.localeCompare(r));
        this.setEnvironment(keys[0]);
    }

    nextEnvironment()
    {
        let keys = Object.keys(this.allHistories).sort((l, r) => l.localeCompare(r));
        this.setEnvironment(keys[(keys.indexOf(this.currentEnvironmentName) + 1) % keys.length]);
    }

    setEnvironment(name: string)
    {
        this.currentEnvironmentName = name;
        if(!this.allHistories.hasOwnProperty(this.currentEnvironmentName))
            return;
        this.environmentChanged$.next({name: this.currentEnvironmentName, environments: this.allHistories});
    }


}