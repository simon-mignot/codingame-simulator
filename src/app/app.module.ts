import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CanvasComponent } from './components/canvas/canvas.component';
import { PlayerComponent } from './components/player/player.component';
import { TestsListComponent } from './components/tests-list/tests-list.component';
import { HttpClientModule } from "@angular/common/http";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { FormsModule } from "@angular/forms";
import { HistoryComponent } from './components/history/history.component';

import { NgTrimPipeModule } from 'angular-pipes';

registerLocaleData(localeFr);

@NgModule({
    declarations: [
        AppComponent,
        CanvasComponent,
        PlayerComponent,
        TestsListComponent,
        HistoryComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        NgbModule,
        FormsModule,
        NgTrimPipeModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
