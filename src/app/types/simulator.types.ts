export type Entity =
{
    id: number | string;
    x: number;
    y: number;
}
export type ZombieEntity = Entity &
{
    nextX: number;
    nextY: number;
}

export type HistoryFrame =
{
    round: number;
    ash: Entity;
    zombies: ZombieEntity[];
    humans: Entity[];

    totalScore: number;
    roundScore: number;
    zombiesKilled: number;
    humansEaten: number;

    scoreData?: ScoreData[];
    logs: { place: string, output: any[] }[];

    ashTarget: any;
}

export type ScoreData =
{
    opacity: number;
    score: number;
    zombiesKilled: number;
    x: number;
    y: number;
    //fibonacciMultiplier: number;
    //humanValue: number;
}

export type SSETestResult =
{
    history: HistoryFrame[];
    result: SSETestResult_result;
}

export type SSETestResult_result =
{
    agent: string;
    environment: string;
    falseScore: number;
    humansLeft: number;
    rounds: number;
    score: number;
    status: "TIMEOUT" | "WIN" | "DIED_OF_OLD_AGE" | "LOST";
    totalHumans: number;
    totalZombies: number;
    zombiesLeft: number;
}
export type HistoryObserverValue = { name: string, environments: { [p: string]: SSETestResult } };

export function isSSETestResult(obj: any): obj is SSETestResult
{
    return obj.hasOwnProperty("history") && obj.hasOwnProperty("result");
}