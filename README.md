# CodeVsZombies

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.1.

## Testing your code

### Presentation of the workspace

The workspace for your code is at ``./src_livecoding/src``. You can create as many file/folder as you want.

For the generated code to be *codingame-friendly* you must not use import at top-level code (it creates a new module if
you do, and will crash both on codingame and on the simulator). To be able to call code from other file, all code from
all files must be within ``namespace livecoding {}``. No need to import anything, but the export keyword is still
required.

The main file is ``./src/main.js``, and ``./src/codingame.d.ts`` is required for the compiler. The output is put
in ``readytosend.js``.

### Running the workspace

``npm run livecoding`` will start the workspace by launching four services:

- ``[SimulationServer]`` The server that will run the simulation of your code on all environments and push the result to
  the angular app.
- ``[SimulationUpdater]`` The watcher for ``readytosend.js``, it will send the required post request to the server on
  code change.
- ``[TSCompiler]`` The typescript compiler.
- ``[ng-serve]`` The angular application.

It will start and display the output of the four services. The one you are interested in is ``[TSCompiler]``, for
obvious reason.

**Work in progress** - If there are error at runtime, they should be displayed on the angular app for corresponding
environment.

## Features

 - Done
   - History
     - Open the IDE when clicking on the file path link
       - Currently, can only open the compiled javascript (not minified) that is on the server side.
 - Todo
   - General
     - Make the components resizable instead of fixed bootstrap columns
     - Canvas and Server:
       - Use console.debug or .dir to add additional information to a frame, to draw directly on the canvas
         - e.g.: `console.debug({ type: 'line', x1: ash.x, y1: ash.y, x2: zombie.x, y2: zombie.y: fill: 'red' })`
         - Might not be able to be animated
     - Save the last code used to LocalStorage 
   - Player
     - Control the player with left/right arrow to navigate through the frames, and space to play/stop
   - Tests list
     - Control the current playing test with up/down arrow
   - History
     - Scroll to the output of the currently displayed frame on the Player
     - Make use of the source map to be able to open the correct typescript file instead of the compiled javascript

### Troubleshooting

 - If the angular app does not update when the typescript compiler outputs a valid new file, reloading the page might correct it by reconnecting the code-push feature.
 - Sometimes the file watcher can be too fast and the server would return all tests in error, just save the file again to correct it.